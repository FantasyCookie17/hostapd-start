# hostapd-start

This is a small systemd service that starts a script (I totally understand if you do not want systemd, in that case you can solely use the script; I even included instructions for Runit) to start hostapd. It will hopefully fail less than the default hostapd service, which has the problem that it doesn't unblock wlan interfaces (see the first line of start.sh for what I did basically). See INSTALL.md for how to install. 

The script assumes the location of your hostapd config is /etc/hostapd.conf, but you may change it to anything you like. The script itself is POSIX-compliant, thus the recommended shell is Dash. Dependencies are a POSIX-compliant shell, hostapd and rfkill. In addition, you'll need systemd for the service unit. Furthermore, please make sure your WLAN interface isn't managed by anything else than hostapd, e.g. NetworkManager, ConnMan or wicd.

Currently code quality etc. is really bad here (though the whole thing is rather minimal), so Pull Requests are appreciated. Maybe we can even achieve that this replaces the default hostapd service, or at least the rfkill change here can be contributed back to upstream.