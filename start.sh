#!/bin/sh
rfkill unblock wlan #This is crucial and the main difference to the default service. Not unblocking wlan interfaces will typically result in the failure of hostapd.
exec hostapd /etc/hostapd.conf #Start hostapd with configuration file /etc/hostapd.conf
